<?php 
// Программа для очистки таблиц
include 'config.php';
$start = microtime(true); //время старта)))
$db = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
echo "Начинаю очистку\n";
$db->query("TRUNCATE TABLE `" . DB_PREFIX . "category`");  // чистим категории
$db->query("TRUNCATE TABLE `" . DB_PREFIX . "category_description`");  // чистим категории
$db->query("TRUNCATE TABLE `" . DB_PREFIX . "category_filter`"); // чистим категории
$db->query("TRUNCATE TABLE `" . DB_PREFIX . "category_path`"); // чистим категории
$db->query("TRUNCATE TABLE `" . DB_PREFIX . "category_to_layout`"); // чистим категории
$db->query("TRUNCATE TABLE `" . DB_PREFIX . "category_to_store`"); // чистим категории
echo "Категории очистили)\n";
echo "Чистим производителей\n";
$db->query("TRUNCATE TABLE `" . DB_PREFIX . "manufacturer`");
$db->query("TRUNCATE TABLE `" . DB_PREFIX . "manufacturer_description`");
$db->query("TRUNCATE TABLE `" . DB_PREFIX . "manufacturer_to_store`");
echo "Произдителей больше нет)\n";
echo "А теперь удаляю товар\n";
$db->query("TRUNCATE TABLE `" . DB_PREFIX . "product`");
$db->query("TRUNCATE TABLE `" . DB_PREFIX . "product_description`");
$db->query("TRUNCATE TABLE `" . DB_PREFIX . "product_filter`");
$db->query("TRUNCATE TABLE `" . DB_PREFIX . "product_image`");
$db->query("TRUNCATE TABLE `" . DB_PREFIX . "product_option`");
$db->query("TRUNCATE TABLE `" . DB_PREFIX . "product_option_value`");
$db->query("TRUNCATE TABLE `" . DB_PREFIX . "product_to_category`");
$db->query("TRUNCATE TABLE `" . DB_PREFIX . "product_to_layout`");
$db->query("TRUNCATE TABLE `" . DB_PREFIX . "product_to_store`");
echo "Готово! Торговать больше нечем ;)\n";




 ?>