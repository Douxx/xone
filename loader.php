<?php 
include 'config.php';
$start = microtime(true); //понты)))
$db = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
$db->query("set_client='utf8'");
$db->query("set character_set_results='utf8'");
$db->query("set collation_connection='utf8_general_ci'");
$db->query("SET NAMES utf8");
$db->set_charset('utf8');
$date = date("Y-m-d H:i:s");
//загружаю файл
$file = simplexml_load_file('https://xn--h1adaobdc5a2b.net/megadb.xml'); //
//var_dump($file);
$x = count($file->Каталог->children());
echo "Привет! Я начинаю загрузку ".$x." товаров ;)";
foreach ($file->Каталог->Товар as $xml) {
// Ищим производителя, если такой уже есть получаем его id если нет создаем
	$manufacturer_id=0; // id производителя
	$category_id = 0; // id категории
	$category_child_1_id = 0; // id дочерней категории уровень 2
	$category_child_2_id = 0; // id дочерней категории уровень 3
	$man = $db->query("SELECT * FROM `" . DB_PREFIX . "manufacturer` WHERE `name`='$xml->Производитель' ");
	if ($man->num_rows) { // Если производитель найденн
		$manAr = $man->fetch_assoc();
		$manufacturer_id=$manAr['manufacturer_id'];
		//echo "Найден ID".$manAr['manufacturer_id']."\n";
	}
	else{  // создаем нового производителя
		$db->query("INSERT INTO `" . DB_PREFIX . "manufacturer` SET `name` = '$xml->Производитель', `sort_order` = '1'");
		$manufacturer_id = $db->insert_id;
		$db->query("INSERT INTO `" . DB_PREFIX . "manufacturer_description` SET `manufacturer_id`='$manufacturer_id', `language_id`='1', `name`='$xml->Производитель',`description`='',`meta_title`='', `meta_h1`='', `meta_description`='', `meta_keyword`=''");
	    $db->query("INSERT INTO `" . DB_PREFIX . "manufacturer_to_store` SET `manufacturer_id` = '$manufacturer_id', `store_id` = '0'");
	}
// ПРОИЗВОДИТЕЛЬ СОЗДАН ИЛИ НАЙДЕН -----------------------------------------------------------------------------------------------------------
	//Ищим категории и если ненашли то создаем.
	//начинаем с главной
	$category = $db->query("SELECT * FROM `" . DB_PREFIX . "category_description` WHERE `name`='$xml->РазделУровень1' ");
	if($category->num_rows){ // Если категория найденна
		$catArray =  $category->fetch_assoc();
		$category_id = $catArray['category_id'];
		
	}
	else{ //ЕСЛИ КАТЕГОРИЯ НЕНАЙДЕННА - СОЗДАЕМ
		$db->query("INSERT INTO `" . DB_PREFIX . "category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES (NULL, '', '0', '1', '1', '0', '1', '$date', '$date')");
	$category_id = $db->insert_id;
		$db->query("INSERT INTO `" . DB_PREFIX . "category_path` (`category_id`, `path_id`, `level`) VALUES ('$category_id', '$category_id', '0')");
		$db->query("INSERT INTO `" . DB_PREFIX . "category_to_layout` (`category_id`, `store_id`, `layout_id`) VALUES ('$category_id', '0', '0')");
		$db->query("INSERT INTO `" . DB_PREFIX . "category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_h1`, `meta_description`, `meta_keyword`) VALUES ('$category_id', '1', '$xml->РазделУровень1','','','','','')");
		$db->query("INSERT INTO `" . DB_PREFIX . "category_to_store` (`category_id`, `store_id`) VALUES ('$category_id', '0')");
		}
		//Ищем дочерние категории 2-й уровень
		$category_child_1 = $db->query("SELECT * FROM `" . DB_PREFIX . "category_description` WHERE `name`='$xml->РазделУровень2' ");
		if($category_child_1->num_rows){ // Если категория найденна
		$catArray =  $category_child_1->fetch_assoc();
		$category_child_1_id = $catArray['category_id'];
		
		}
		else{ //если дочерняя категория второго уровня не найденна, создаем 
		$db->query("INSERT INTO `" . DB_PREFIX . "category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES (NULL, '', '$category_id', '0', '1', '0', '1', '$date', '$date')");
		$category_child_1_id = $db->insert_id;
		$db->query("INSERT INTO `" . DB_PREFIX . "category_path` (`category_id`, `path_id`, `level`) VALUES ('$category_child_1_id', '$category_id', '0')");
		$db->query("INSERT INTO `" . DB_PREFIX . "category_path` (`category_id`, `path_id`, `level`) VALUES ('$category_child_1_id', '$category_child_1_id', '1')");
		$db->query("INSERT INTO `" . DB_PREFIX . "category_to_layout` (`category_id`, `store_id`, `layout_id`) VALUES ('$category_child_1_id', '0', '0')");
		$db->query("INSERT INTO `" . DB_PREFIX . "category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_h1`, `meta_description`, `meta_keyword`) VALUES ('$category_child_1_id', '1', '$xml->РазделУровень2','','','','','')");
		$db->query("INSERT INTO `" . DB_PREFIX . "category_to_store` (`category_id`, `store_id`) VALUES ('$category_child_1_id', '0')");

		}

		//Ищем дочерние категории 3-й уровень
		$category_child_2 = $db->query("SELECT * FROM `" . DB_PREFIX . "category_description` WHERE `name`='$xml->РазделУровень3' ");
		if($category_child_2->num_rows){ // Если категория найденна
		$catArray =  $category_child_2->fetch_assoc();
		$category_child_2_id = $catArray['category_id'];
		}
		else{ //если дочерняя категория второго уровня не найденна, создаем 
		$db->query("INSERT INTO `" . DB_PREFIX . "category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES (NULL, '', '$category_child_1_id', '0', '1', '0', '1', '$date', '$date')");
		$category_child_2_id = $db->insert_id;
		$db->query("INSERT INTO `" . DB_PREFIX . "category_path` (`category_id`, `path_id`, `level`) VALUES ('$category_child_2_id', '$category_id', '0')");
		$db->query("INSERT INTO `" . DB_PREFIX . "category_path` (`category_id`, `path_id`, `level`) VALUES ('$category_child_2_id', '$category_child_1_id', '1')");
		$db->query("INSERT INTO `" . DB_PREFIX . "category_path` (`category_id`, `path_id`, `level`) VALUES ('$category_child_2_id', '$category_child_2_id', '2')");
		$db->query("INSERT INTO `" . DB_PREFIX . "category_to_layout` (`category_id`, `store_id`, `layout_id`) VALUES ('$category_child_2_id', '0', '0')");
		$db->query("INSERT INTO `" . DB_PREFIX . "category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_h1`, `meta_description`, `meta_keyword`) VALUES ('$category_child_2_id', '1', '$xml->РазделУровень3','','','','','')");
		$db->query("INSERT INTO `" . DB_PREFIX . "category_to_store` (`category_id`, `store_id`) VALUES ('$category_child_2_id', '0')");

		}

		// Теперь когда созданны категории можно создавать товар
		//Используем следующие переменные 
		//$manufacturer_id  - id производителя
		//$category_id  - id категории
		//$category_child_1_id - id дочерней категории уровень 2
		// товар создаем без проверок на существование
		$image ="catalog/".$xml->ОсновноеФото;
		$db->query("INSERT INTO `" . DB_PREFIX . "product` (`product_id`, `model`,`sku`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `stock_status_id`, `image`, `manufacturer_id`, `shipping`, `price`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `viewed`, `date_added`, `date_modified`) VALUES (NULL, '$xml->Артикул','','','','','','','', '0', '5', '$image', '$manufacturer_id', '1', '$xml->РозничнаяЦена', '0', '0', '$date', '0.00', '0', '0.00', '0.00', '0.00', '0', '1', '1', '0', '1', '0', '$date', '$date')");
		$product_id=$db->insert_id;
		$db->query("INSERT INTO `" . DB_PREFIX . "product_description` (`product_id`, `language_id`, `name`, `description`, `tag`, `meta_title`, `meta_h1`, `meta_description`, `meta_keyword`) VALUES ('$product_id', '1', '$xml->Наименование', '$xml->Описание', '', '', '', '', '')");

		$db->query("INSERT INTO `" . DB_PREFIX . "product_to_category` (`product_id`, `category_id`, `main_category`) VALUES ('$product_id', '$category_id', '0')");
		$db->query("INSERT INTO `" . DB_PREFIX . "product_to_category` (`product_id`, `category_id`, `main_category`) VALUES ('$product_id', '$category_child_1_id', '1')");
		$db->query("INSERT INTO `" . DB_PREFIX . "product_to_category` (`product_id`, `category_id`, `main_category`) VALUES ('$product_id', '$category_child_2_id', '0')");
		$db->query("INSERT INTO `" . DB_PREFIX . "product_to_layout` (`product_id`, `store_id`, `layout_id`) VALUES ('$product_id', '0', '0')");
		$db->query("INSERT INTO `" . DB_PREFIX . "product_to_store` (`product_id`, `store_id`) VALUES ('$product_id', '0')");
		

}
$time = round(microtime(true) - $start,3);
echo "Обновление успешно выполненно за ".$time."\n";

?>
